/**
 *       @file  BackpropOCL_exc.cc
 *      @brief  The BackpropOCL BarbequeRTRM application
 *
 * Porting of the Rodinia OpenCL benchmark sample
 *
 *     @author  Giuseppe Massari, giuseppe.massari@polimi.it
 *
 *     Company  Politecnico di Milano
 * 		University of Virginia
 *   Copyright  Copyright (c) 2015
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#ifndef BACKPROPOCL_EXC_H_
#define BACKPROPOCL_EXC_H_

#include <bbque/bbque_exc.h>

#include "backprop.h"

#ifndef CONFIG_BBQUE_OPENCL
#include <CL/cl.h>
#endif

using bbque::rtlib::BbqueEXC;

class BackpropOCL : public BbqueEXC {

public:

	BackpropOCL(std::string const & name,
			std::string const & recipe,
			RTLIB_Services_t *rtlib,
			BPNN *net, float *eo, float *eh, int nr_iter, int awmid = -1);

private:

	RTLIB_ExitCode_t onSetup();
	RTLIB_ExitCode_t onConfigure(int8_t awm_id);
	RTLIB_ExitCode_t onRun();
	RTLIB_ExitCode_t onMonitor();
	RTLIB_ExitCode_t onRelease();
	void Cleanup();

	cl_context       context;
	cl_command_queue cmd_queue;
	cl_device_type   device_type = CL_DEVICE_TYPE_ALL;
	cl_device_id   * device_list;
	cl_int           num_devices;

	BPNN  * net;
	float * eo;
	float * eh;
	int nr_iterations;

	int awm_id;

	int in;
	int hid;
	int out;
	float out_err;
	float hid_err;

	char * kernel_bp1  = "bpnn_layerforward_ocl";
	char * kernel_bp2  = "bpnn_adjust_weights_ocl";
	char * source;
	cl_kernel kernel1;
	cl_kernel kernel2;

	float * input_weights_one_dim;
	float * input_weights_prev_one_dim;
	float * partial_sum;
	float sum;
	float num_blocks;

	size_t global_work[3];
	size_t local_work[3];

	// Memory buffers
	cl_mem input_hidden_ocl;
	cl_mem input_ocl;
	cl_mem output_hidden_ocl;
	cl_mem hidden_partial_sum;
	cl_mem hidden_delta_ocl;
	cl_mem input_prev_weights_ocl;
};

#endif // BACKPROPOCL_EXC_H_

