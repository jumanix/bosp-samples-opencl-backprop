
ifdef CONFIG_SAMPLES_OPENCL_BACKPROP

# Targets provided by this project
.PHONY: samples_opencl_backprop clean_samples_opencl_backprop

# Add this to the "contrib_testing" target
samples_opencl: samples_opencl_backprop
clean_samples_opencl: clean_samples_opencl_backprop

MODULE_SAMPLES_OPENCL_BACKPROPOCL=samples/opencl/backprop

samples_opencl_backprop:
	@echo
	@echo "==== Building BackpropOCL ($(BUILD_TYPE)) ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(BOSP_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@[ -d $(MODULE_SAMPLES_OPENCL_BACKPROPOCL)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_SAMPLES_OPENCL_BACKPROPOCL)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_SAMPLES_OPENCL_BACKPROPOCL)/build/$(BUILD_TYPE) && \
		CC=$(CC) CFLAGS=$(TARGET_FLAGS) \
		CXX=$(CXX) CXXFLAGS=$(TARGET_FLAGS) \
		cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_SAMPLES_OPENCL_BACKPROPOCL)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_samples_opencl_backprop:
	@echo
	@echo "==== Clean-up BackpropOCL Application ===="
	@rm -rf $(MODULE_SAMPLES_OPENCL_BACKPROPOCL)/build
	@echo

run-ocl-backprop:
	@echo "==== Running BackpropOCL Application ===="
	${BOSP_SYSROOT}/usr/bin/backpropocl 65536 1000


else # CONFIG_SAMPLES_OPENCL_BACKPROPOCL

samples_opencl_backprop:
	$(warning Backprop disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_SAMPLES_OPENCL_BACKPROP

