/**
 *       @file  BackpropOCL_exc.cc
 *      @brief  The BackpropOCL BarbequeRTRM application
 *
 * Porting of the Rodinia OpenCL benchmark sample
 *
 *     @author  Giuseppe Massari, giuseppe.massari@polimi.it
 *
 *     Company  Politecnico di Milano
 * 		University of Virginia
 *   Copyright  Copyright (c) 2015
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#include <CL/cl.h>
#include <cstring>
#include <cstdio>
#include <math.h>

#include "BackpropOCL_exc.h"

#include <bbque/utils/utility.h>


extern void bpnn_initialize(int);
extern BPNN * bpnn_create(int, int, int);
extern void load(BPNN*);
extern void bpnn_free(BPNN*);


BackpropOCL::BackpropOCL(std::string const & name,
		std::string const & recipe,
		RTLIB_Services_t *rtlib,
		BPNN *_net, float *_eo, float *_eh, int nr_iter, int awmid) :
	net(_net), eo(_eo), eh(_eh), nr_iterations(nr_iter), awm_id(awmid),
	BbqueEXC(name, recipe, rtlib, RTLIB_LANG_OPENCL) {

	logger->Warn("New BackpropOCL::BackpropOCL()");
	logger->Info("EXC Unique IDentifier (UID): %u", GetUid());
}

RTLIB_ExitCode_t BackpropOCL::onSetup() {
	logger->Warn("BackpropOCL::onSetup()");
	in  = net->input_n;
	hid = net->hidden_n;
	out = net->output_n;

	num_blocks = in / BLOCK_SIZE;

	int sourcesize = 1024*1024;
	source = (char *) calloc(sourcesize, sizeof(char));
	if(!source) {
		logger->Error("ERROR: calloc(%d) failed\n", sourcesize);
		return RTLIB_ERROR;
	}

	// read the kernel core source
	char * tempchar = {
		BBQUE_PATH_PREFIX """/usr/bin/backprop_kernel/backprop_kernel.cl" };
	FILE * fp = fopen(tempchar, "rb");
	if(!fp) {
		logger->Error("ERROR: unable to open '%s'\n", tempchar);
		return RTLIB_ERROR;
	}
	fread(source + strlen(source), sourcesize, 1, fp);
	fclose(fp);

	// Force AWM selection
	if (awm_id >= 0) {
		RTLIB_Constraint_t constr_set = {
			awm_id,
			CONSTRAINT_ADD,
			LOWER_BOUND
		};
		SetConstraints(&constr_set, 1);
		logger->Notice("Forcing Application Working Mode [%2d]", awm_id);
	}
	// Force AWM selection
	if (awm_id >= 0) {
		RTLIB_Constraint_t constr_set = {
			awm_id,
			CONSTRAINT_ADD,
			UPPER_BOUND
		};
		SetConstraints(&constr_set, 1);
		logger->Notice("Forcing Application Working Mode [%2d]", awm_id);
	}

	return RTLIB_OK;
}

RTLIB_ExitCode_t BackpropOCL::onConfigure(int8_t awm_id) {
	cl_int result;
	size_t size;

	logger->Warn("BackpropOCL::onConfigure(): EXC [%s] => AWM [%02d]",
		exc_name.c_str(), awm_id);
	if (Cycles() > 1) {
		logger->Warn("Cleaning previous context...");
		Cleanup();
	}

	/**********************************************************************
	 *  Device, context, queue
	 **********************************************************************/

	cl_platform_id platform_id;
	if (clGetPlatformIDs(1, &platform_id, NULL) != CL_SUCCESS) {
		logger->Error("ERROR: clGetPlatformIDs(1,*,0) failed\n");
		return RTLIB_ERROR;
	}

	cl_context_properties ctxprop[] = {
		CL_CONTEXT_PLATFORM, (cl_context_properties)platform_id, 0
	};
	context = clCreateContextFromType( ctxprop, CL_DEVICE_TYPE_ALL, NULL, NULL, NULL );
	if( !context ) {
		logger->Error("ERROR: clCreateContextFromType failed");
		return RTLIB_ERROR;
	}

	result = clGetContextInfo( context, CL_CONTEXT_DEVICES, 0, NULL, &size );

	num_devices = (int) (size / sizeof(cl_device_id));
	logger->Info("Number of devices = %d\n", num_devices);
	if( result != CL_SUCCESS || num_devices < 1 ) {
		logger->Error("ERROR: clGetContextInfo() failed\n");
		return RTLIB_ERROR;
	}
	device_list = new cl_device_id[num_devices];
	if( !device_list ) {
		logger->Error("ERROR: new cl_device_id[] failed\n");
		return RTLIB_ERROR;
	}
	result = clGetContextInfo( context, CL_CONTEXT_DEVICES, size, device_list, NULL );
	if( result != CL_SUCCESS ) {
		logger->Error("ERROR: clGetContextInfo() failed\n");
		return RTLIB_ERROR;
	}

	// create command queue for the first device
	cmd_queue = clCreateCommandQueue( context, device_list[0], 0, NULL );
	if( !cmd_queue ) {
		logger->Error("ERROR: clCreateCommandQueue() failed\n");
		return RTLIB_ERROR;
	}

	/**********************************************************************
	 *  Compile kernel
	 **********************************************************************/

	cl_int err = 0;
	const char * slist[2] = { source, 0 };
	cl_program prog = clCreateProgramWithSource(context, 1, slist, NULL, &err);
	if(err != CL_SUCCESS) {
		logger->Error("ERROR: clCreateProgramWithSource() => %d\n", err);
		return RTLIB_ERROR;
	}
	err = clBuildProgram(prog, 0, NULL, NULL, NULL, NULL);
	if(err != CL_SUCCESS) {
		logger->Error("ERROR: clBuildProgram() => %d\n", err);
		return RTLIB_ERROR;
	}

	kernel1 = clCreateKernel(prog, kernel_bp1, &err);
	kernel2 = clCreateKernel(prog, kernel_bp2, &err);
	if(err != CL_SUCCESS) {
		logger->Error("ERROR: clCreateKernel() 0 => %d\n", err);
		return RTLIB_ERROR;
	}
	clReleaseProgram(prog);

	input_weights_one_dim = (float *) malloc((in + 1)* (hid + 1) * sizeof(float));
	input_weights_prev_one_dim = (float *) malloc((in + 1)* (hid + 1) * sizeof(float));
	partial_sum = (float *) malloc(num_blocks * WIDTH * sizeof(float));

	// set global and local workitems
	global_work[0] = BLOCK_SIZE;
	global_work[1] = BLOCK_SIZE * num_blocks;
	global_work[2] = 1;
	local_work[0] = BLOCK_SIZE;
	local_work[1] = BLOCK_SIZE;
	local_work[2] = 1;
	logger->Notice("2-D NDRange: %d x %.0f work-groups. Work-group size: %d x %d",
		BLOCK_SIZE, BLOCK_SIZE * num_blocks, BLOCK_SIZE, BLOCK_SIZE);

	// this preprocessing stage is temporarily added to correct the bug of
	// wrong memcopy using two-dimensional net->inputweights
	// todo: fix mem allocation
	int m = 0;
	for (int k = 0; k <= in; k++) {
		for (int j = 0; j <= hid; j++) {
			input_weights_one_dim[m] = net->input_weights[k][j];
			input_weights_prev_one_dim[m] = net-> input_prev_weights[k][j];
			m++;
		}
	}

	/**********************************************************************
	 *  Memory buffers allocation
	 **********************************************************************/

	input_ocl = clCreateBuffer(context, CL_MEM_READ_WRITE, (in + 1) * sizeof(float), NULL, &err );
	if(err != CL_SUCCESS) {
		logger->Error("ERROR: clCreateBuffer input_ocl\n");
		return RTLIB_ERROR;
	}
	input_hidden_ocl =
		clCreateBuffer(context, CL_MEM_READ_WRITE, (in + 1) * (hid + 1) * sizeof(float), NULL, &err );
	if(err != CL_SUCCESS) {
		logger->Error("ERROR: clCreateBuffer input_hidden_ocl\n");
		return RTLIB_ERROR;
	}
	output_hidden_ocl =
		clCreateBuffer(context, CL_MEM_READ_WRITE, (hid + 1) * sizeof(float), NULL, &err );
	if(err != CL_SUCCESS) {
		logger->Error("ERROR: clCreateBuffer output_hidden_ocl\n");
		return RTLIB_ERROR;}
	hidden_partial_sum =
		clCreateBuffer(context, CL_MEM_READ_WRITE, num_blocks * WIDTH * sizeof(float), NULL, &err );
	if(err != CL_SUCCESS) {
		logger->Error("ERROR: clCreateBuffer hidden_partial_sum\n");
		return RTLIB_ERROR;
	}
	hidden_delta_ocl = clCreateBuffer(context, CL_MEM_READ_WRITE, (hid + 1) * sizeof(float), NULL, &err );
	if(err != CL_SUCCESS) {
		logger->Error("ERROR: clCreateBuffer hidden_delta_ocl\n");
		return RTLIB_ERROR;
	}
	input_prev_weights_ocl =
		clCreateBuffer(context, CL_MEM_READ_WRITE, (in + 1) * (hid + 1) * sizeof(float), NULL, &err );
	if(err != CL_SUCCESS) {
		logger->Error("ERROR: clCreateBuffer input_prev_weights_ocl\n");
		return RTLIB_ERROR;
	}

	return RTLIB_OK;
}

RTLIB_ExitCode_t BackpropOCL::onRun() {
	RTLIB_WorkingModeParams_t const wmp = WorkingModeParams();
	cl_int err;

	// Return after nr_iterations
	if (Cycles() >= nr_iterations)
		return RTLIB_EXC_WORKLOAD_NONE;

	logger->Notice("BackpropOCL::onRun()  : EXC [%s]  @ AWM [%02d], Cycle [%4d]",
		exc_name.c_str(), wmp.awm_id, Cycles());

	//write buffers
	err = clEnqueueWriteBuffer(cmd_queue, input_ocl, 1, 0, (in + 1) * sizeof(float), net->input_units, 0, 0, 0);
	if(err != CL_SUCCESS) {
		logger->Error("ERROR: clEnqueueWriteBuffer input_ocl\n");
		return RTLIB_ERROR;
	}
	err = clEnqueueWriteBuffer(cmd_queue, input_hidden_ocl, 1, 0, (in + 1) * (hid + 1) * sizeof(float), input_weights_one_dim, 0, 0, 0);
	if(err != CL_SUCCESS) {
		logger->Error("ERROR: clEnqueueWriteBuffer input_hidden_ocl\n");
		return RTLIB_ERROR;
	}

	clSetKernelArg(kernel1, 0, sizeof(void *), (void*) &input_ocl);
	clSetKernelArg(kernel1, 1, sizeof(void *), (void*) &output_hidden_ocl);
	clSetKernelArg(kernel1, 2, sizeof(void *), (void*) &input_hidden_ocl);
	clSetKernelArg(kernel1, 3, sizeof(void *), (void*) &hidden_partial_sum );
	clSetKernelArg(kernel1, 4, sizeof(float) *  HEIGHT, (void*)NULL );
	clSetKernelArg(kernel1, 5, sizeof(float ) *  HEIGHT * WIDTH, (void*)NULL );
	clSetKernelArg(kernel1, 6, sizeof(cl_int), (void*) &in);
	clSetKernelArg(kernel1, 7, sizeof(cl_int), (void*) &hid);

	err = clEnqueueNDRangeKernel(cmd_queue, kernel1, 2, NULL, global_work, local_work, 0, 0, 0);
	if(err != CL_SUCCESS) {
		logger->Error("ERROR: 1  clEnqueueNDRangeKernel()=>%d failed\n", err);
		return RTLIB_ERROR;
	}

	err = clEnqueueReadBuffer(cmd_queue, hidden_partial_sum, 1, 0, num_blocks * WIDTH * sizeof(float), partial_sum, 0, 0, 0);
	if(err != CL_SUCCESS) {
		logger->Error("ERROR: 1  clEnqueueReadBuffer: partial sum\n");
		return RTLIB_ERROR;
	}

	for (int j = 1; j <= hid; j++) {
		sum = 0.0;
		for (int k = 0; k < num_blocks; k++) {
			sum += partial_sum[k * hid + j-1] ;
		}
		sum += net->input_weights[0][j];
		net-> hidden_units[j] = float(1.0 / (1.0 + exp(-sum)));
	}


	bpnn_layerforward(net->hidden_units, net->output_units, net->hidden_weights, hid, out);
	bpnn_output_error(net->output_delta, net->target, net->output_units, out, &out_err);
	bpnn_hidden_error(net->hidden_delta, hid, net->output_delta, out, net->hidden_weights, net->hidden_units, &hid_err);
	bpnn_adjust_weights(net->output_delta, out, net->hidden_units, hid, net->hidden_weights, net->hidden_prev_weights);

	err = clEnqueueWriteBuffer(cmd_queue, hidden_delta_ocl,       1, 0, (hid + 1) * sizeof(float), net->hidden_delta, 0, 0, 0);
	if(err != CL_SUCCESS) {
		logger->Error("ERROR: clEnqueueWriteBuffer hidden_delta_ocl\n");
		return RTLIB_ERROR;
	}
	err = clEnqueueWriteBuffer(cmd_queue, input_prev_weights_ocl, 1, 0, (in + 1) * (hid + 1) * sizeof(float), input_weights_prev_one_dim, 0, 0, 0);
	if(err != CL_SUCCESS) {
		logger->Error("ERROR: clEnqueueWriteBuffer input_prev_weights_ocl\n");
		return RTLIB_ERROR;
	}
	err = clEnqueueWriteBuffer(cmd_queue, input_hidden_ocl,       1, 0, (in + 1) * (hid + 1) * sizeof(float), input_weights_one_dim, 0, 0, 0);
	if(err != CL_SUCCESS) {
		logger->Error("ERROR: clEnqueueWriteBuffer input_hidden_ocl\n");
		return RTLIB_ERROR;
	}

	clSetKernelArg(kernel2, 0, sizeof(void *), (void*) &hidden_delta_ocl);
	clSetKernelArg(kernel2, 1, sizeof(cl_int), (void*) &hid);
	clSetKernelArg(kernel2, 2, sizeof(void *), (void*) &input_ocl);
	clSetKernelArg(kernel2, 3, sizeof(cl_int), (void*) &in);
	clSetKernelArg(kernel2, 4, sizeof(void *), (void*) &input_hidden_ocl);
	clSetKernelArg(kernel2, 5, sizeof(void *), (void*) &input_prev_weights_ocl );

	err = clEnqueueNDRangeKernel(cmd_queue, kernel2, 2, NULL, global_work, local_work, 0, 0, 0);
	if(err != CL_SUCCESS) {
		logger->Error("ERROR: 1  clEnqueueNDRangeKernel()=>%d failed\n", err);
		return RTLIB_ERROR;
	}

	err = clEnqueueReadBuffer(cmd_queue, input_ocl, 1, 0, (in + 1) * sizeof(float), net->input_units, 0, 0, 0);
	if(err != CL_SUCCESS) {
		logger->Error("ERROR: 1  clEnqueueReadBuffer: input_ocl\n");
		return RTLIB_ERROR;
	}
	err = clEnqueueReadBuffer(
		cmd_queue, input_hidden_ocl, 1, 0, (in + 1) * (hid + 1) * sizeof(float), input_weights_one_dim, 0, 0, 0);
	if(err != CL_SUCCESS) {
		logger->Error("ERROR: 1  clEnqueueReadBuffer: input_hidden_ocl\n");
		return RTLIB_ERROR;
	}

	return RTLIB_OK;
}

RTLIB_ExitCode_t BackpropOCL::onMonitor() {
	RTLIB_WorkingModeParams_t const wmp = WorkingModeParams();

	logger->Info("BackpropOCL::onMonitor()  : EXC [%s]  @ AWM [%02d], Cycle [%4d]",
		exc_name.c_str(), wmp.awm_id, Cycles());

	return RTLIB_OK;
}

RTLIB_ExitCode_t BackpropOCL::onRelease() {

	Cleanup();
	free(source);
	return RTLIB_OK;
}

void BackpropOCL::Cleanup() {
	if( cmd_queue ) clReleaseCommandQueue( cmd_queue );
	if( context ) clReleaseContext( context );
	if( device_list ) delete[] device_list;

	cmd_queue   = 0;
	context     = 0;
	device_list = 0;
	num_devices = 0;
	device_type = 0;

	clReleaseMemObject(input_ocl);
	clReleaseMemObject(output_hidden_ocl);
	clReleaseMemObject(input_hidden_ocl);
	clReleaseMemObject(hidden_partial_sum);
	clReleaseMemObject(input_prev_weights_ocl);
}
