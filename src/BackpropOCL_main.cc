/**
 *       @file  BackpropOCL_exc.cc
 *      @brief  The BackpropOCL BarbequeRTRM application
 *
 * Porting of the Rodinia OpenCL benchmark sample
 *
 *     @author  Giuseppe Massari, giuseppe.massari@polimi.it
 *
 *     Company  Politecnico di Milano
 * 		University of Virginia
 *   Copyright  Copyright (c) 2015
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#include <cstdio>
#include <cstring>
#include <iostream>
#include <random>
#include <memory>
#include <math.h>
#include <libgen.h>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

#include "version.h"
#include "BackpropOCL_exc.h"
#include "backprop.h"

#include <bbque/utils/utility.h>
#include <bbque/utils/logging/logger.h>

// Setup logging
#undef  BBQUE_LOG_MODULE
#define BBQUE_LOG_MODULE "BackpropOCL"

namespace bu = bbque::utils;
namespace po = boost::program_options;

/**
 * @brief A pointer to an EXC
 */
std::unique_ptr<bu::Logger> logger;

/**
 * @brief A pointer to an EXC
 */
typedef std::shared_ptr<BbqueEXC> pBbqueEXC_t;

/**
 * The decription of each BackpropOCL parameters
 */
po::options_description opts_desc("BackpropOCL Configuration Options");

/**
 * The map of all BackpropOCL parameters values
 */
po::variables_map opts_vm;

/**
 * The services exported by the RTLib
 */
RTLIB_Services_t *rtlib;

/**
 * @brief The application configuration file
 */
std::string conf_file = BBQUE_PATH_PREFIX "/" BBQUE_PATH_CONF "/BackpropOCL.conf" ;

/**
 * @brief The recipe to use for all the EXCs
 */
std::string recipe;

/**
 * @brief The EXecution Context (EXC) registered
 */
pBbqueEXC_t pexc;


int layer_size = 0;
int nr_iterations = 1;

extern void bpnn_initialize(int);
extern BPNN * bpnn_create(int, int, int);
extern void bpnn_free(BPNN*);

void load(BPNN *net) {
	float *units;
	int nr = layer_size;
	int nc;
	int imgsize, i, j, k;

	imgsize = nr * nc;
	units = net->input_units;

	k = 1;
	for (i = 0; i < nr; i++) {
		units[k] = (float) rand()/RAND_MAX ;
		k++;
	}
}


void ParseCommandLine(int argc, char *argv[]) {

	if ((argc==1)|| (argc<3)) {
		 fprintf(stderr, "usage: backprop <num of input elements> [iterations]\n");
		 exit(0);
	}
	layer_size = atoi(argv[1]);
	if (layer_size%16!=0){
		 fprintf(stderr, "The number of input points must be divided by 16\n");
		 exit(0);
	}
	if (argc >= 3) {
		 nr_iterations = atoi(argv[2]);
	}

	// Parse command line params
	try {
	po::store(po::parse_command_line(argc, argv, opts_desc), opts_vm);
	} catch(...) {
		std::cout << "Usage: backprop <num of input elements> [iterations]\n";
		std::cout << opts_desc << std::endl;
		::exit(EXIT_FAILURE);
	}
	po::notify(opts_vm);

	// Check for help request
	if (opts_vm.count("help")) {
		std::cout << "Usage: " << argv[0] << " [options]\n";
		std::cout << opts_desc << std::endl;
		::exit(EXIT_SUCCESS);
	}

	// Check for version request
	if (opts_vm.count("version")) {
		std::cout << "BackpropOCL (ver. " << g_git_version << ")\n";
		std::cout << "Copyright (C) 2011 Politecnico di Milano\n";
		std::cout << "\n";
		std::cout << "Built on " <<
			__DATE__ << " " <<
			__TIME__ << "\n";
		std::cout << "\n";
		std::cout << "This is free software; see the source for "
			"copying conditions.  There is NO\n";
		std::cout << "warranty; not even for MERCHANTABILITY or "
			"FITNESS FOR A PARTICULAR PURPOSE.";
		std::cout << "\n" << std::endl;
		::exit(EXIT_SUCCESS);
	}
}

int main(int argc, char *argv[]) {
	BPNN *net;
	float out_err, hid_err;
	int awm = -1;

	opts_desc.add_options()
		("help,h", "print this help message")
		("version,v", "print program version")

		("conf,C", po::value<std::string>(&conf_file)->
			default_value(conf_file),
			"BackpropOCL configuration file")

		("recipe,r", po::value<std::string>(&recipe)->
			default_value("BackpropOCL"),
			"recipe name (for all EXCs)")
		("awm,w", po::value<int>(&awm)->
			default_value(-1),
			"AWM id to force")
	;

	// Setup a logger
	bu::Logger::SetConfigurationFile(conf_file);
	logger = bu::Logger::GetLogger("backpropocl");

	ParseCommandLine(argc, argv);

	// Welcome screen
	logger->Info(".:: BackpropOCL (ver. %s) ::.", g_git_version);
	logger->Info("Built: " __DATE__  " " __TIME__);

	// Input data initialization
	int seed = 7;
	bpnn_initialize(seed);
	net = bpnn_create(layer_size, 16, 1); // (16, 1 can not be changed)
	logger->Info("Input layer size : %d\n", layer_size);
	load(net);

	// Initializing the RTLib library and setup the communication channel
	// with the Barbeque RTRM
	logger->Info("STEP 0. Initializing RTLib, application [%s]...",
			::basename(argv[0]));
	RTLIB_Init(::basename(argv[0]), &rtlib);
	assert(rtlib);

	logger->Info("STEP 1. Registering EXC using [%s] recipe...",
			recipe.c_str());
	pexc = pBbqueEXC_t(new BackpropOCL("BackpropOCL", recipe, rtlib,
		net, &out_err, &hid_err, nr_iterations, awm));
	if (!pexc->isRegistered())
		return RTLIB_ERROR;

	logger->Info("STEP 2. Starting EXC control thread...");
	logger->Info("Starting training kernel...");
	pexc->Start();


	logger->Info("STEP 3. Waiting for EXC completion...");
	pexc->WaitCompletion();


	logger->Info("STEP 4. Disabling EXC...");
	pexc = NULL;
	bpnn_free(net);

	logger->Info("Finish the training for %d iteration(s)", nr_iterations);
	logger->Info("===== BackpropOCL DONE! =====");
	return EXIT_SUCCESS;

}

